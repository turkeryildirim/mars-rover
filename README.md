### mars rover
Input is given from the ```resource/mission.txt``` file.

Application is run by ```main``` method in ```Application.class```.

Input:
```
5 5
1 2 N
L M L M L M L M M
3 3 E
M M R M M R M R R M
```

Expected Output:
```
1 3 N
5 1 E
```