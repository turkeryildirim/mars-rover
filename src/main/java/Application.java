import builders.RoverBuilder;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import models.Mission;
import models.Plateau;
import models.Rover;
import builders.MissionBuilder;

public class Application {
  public static void main(String[] args) throws IOException {
    List<String> lines = Files.readAllLines(Paths.get("src/main/resources/mission.txt"));

    MissionBuilder missionBuilder = new MissionBuilder(lines);

    Plateau plateau = missionBuilder.getPlateau();

    for (Mission mission : missionBuilder.getMissions()) {
      List<String> commands = mission.getCommands();

      RoverBuilder roverBuilder =
          new RoverBuilder(mission.getStartX(), mission.getStartY(), mission.getStartDirection());

      commands.forEach(roverBuilder::move);

      Rover rover = roverBuilder.getRover();

      if ((rover.getX() > plateau.getHeight()) || (rover.getY() > plateau.getWidth())) {
        System.out.println("Rover fell off the plateau!");
        continue;
      }

      System.out.println(rover.getLocationAsString());
    }
  }
}
