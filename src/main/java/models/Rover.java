package models;

public class Rover {

  private int x;
  private int y;
  private String direction;

  public Rover(int x, int y, String direction) {
    this.x = x;
    this.y = y;
    this.direction = direction;
  }

  public int getX() {
    return x;
  }

  public void increaseX() {
    this.x += 1;
  }

  public void decreaseX() {
    this.x -= 1;
  }

  public int getY() {
    return y;
  }

  public void increaseY() {
    this.y += 1;
  }

  public void decreaseY() {
    this.y -= 1;
  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  public String getLocationAsString() {
    return this.x + " " + this.y + " " + this.direction;
  }
}
