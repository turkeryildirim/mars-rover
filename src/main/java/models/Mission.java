package models;

import java.util.List;

public class Mission {

  private final int startX;
  private final int startY;
  private final String startDirection;
  private final List<String> commands;

  public Mission(int startX, int startY, String startDirection, List<String> commands) {
    this.startX = startX;
    this.startY = startY;
    this.startDirection = startDirection;
    this.commands = commands;
  }

  public int getStartX() {
    return startX;
  }

  public int getStartY() {
    return startY;
  }

  public String getStartDirection() {
    return startDirection;
  }

  public List<String> getCommands() {
    return commands;
  }
}
