package builders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import models.Mission;
import models.Plateau;

public class MissionBuilder {

  private final Plateau plateau;
  private final List<Mission> missions = new ArrayList<>();

  public MissionBuilder(List<String> lines) {

    if (lines.size() < 3) {
      throw new IllegalArgumentException("Mission must be at least 3 lines." +
          " || lines length => " + lines.size());
    }

    plateau = createPlateau(lines.get(0));

    for (int i = 1, linesSize = lines.size(); i < linesSize; i += 2) {
      List<String> start = getStartLocation(lines.get(i));
      List<String> commands = createCommand(lines.get(i + 1));

      Mission mission = new Mission(
          Integer.parseInt(start.get(0)),
          Integer.parseInt(start.get(1)),
          start.get(2),
          commands
      );

      missions.add(mission);
    }
  }

  public Plateau getPlateau() {
    return plateau;
  }

  public List<Mission> getMissions() {
    return missions;
  }

  private Plateau createPlateau(String line) {
    List<String> plateauInfo = Arrays.asList(line.trim().replace(" ", "").split(""));

    if (plateauInfo.size() != 2) {
      throw new IllegalArgumentException("Plateau information is missing or corrupt." +
          " || plateauInfo => " + plateauInfo);
    }

    return new Plateau(Integer.parseInt(plateauInfo.get(0)), Integer.parseInt(plateauInfo.get(1)));
  }

  private List<String> getStartLocation(String line) {
    List<String> roverInfo = Arrays.asList(line.trim().replace(" ", "").split(""));

    if (roverInfo.size() != 3) {
      throw new IllegalArgumentException("Rover information is missing or corrupt." +
          " || roverInfo => " + roverInfo);
    }

    return roverInfo;
  }

  private List<String> createCommand(String line) {
    return Arrays.asList(line.trim().replace(" ", "").split(""));
  }
}
