package builders;

import java.util.HashMap;
import java.util.Map;
import models.Rover;

public class RoverBuilder {

  private Rover rover;

  private final Map<String, Runnable> moveForward = new HashMap<>() {{
    put("N", () -> rover.increaseY());
    put("W", () -> rover.decreaseX());
    put("S", () -> rover.decreaseY());
    put("E", () -> rover.increaseX());
  }};

  private final Map<String, Runnable> moveRight = new HashMap<>() {{
    put("N", () -> rover.setDirection("E"));
    put("E", () -> rover.setDirection("S"));
    put("S", () -> rover.setDirection("W"));
    put("W", () -> rover.setDirection("N"));
  }};

  private final Map<String, Runnable> moveLeft = new HashMap<>() {{
    put("N", () -> rover.setDirection("W"));
    put("W", () -> rover.setDirection("S"));
    put("S", () -> rover.setDirection("E"));
    put("E", () -> rover.setDirection("N"));
  }};

  public RoverBuilder(int x, int y, String direction) {
    this.rover = new Rover(x, y, direction);
  }

  public void move(String input) {
    if (input == null) {
      throw new IllegalArgumentException("Unexpected input. Key could not be null.");
    }

    switch (input) {
      case "R":
        this.moveRight.get(this.rover.getDirection()).run();
        break;
      case "L":
        this.moveLeft.get(this.rover.getDirection()).run();
        break;
      case "M":
        this.moveForward.get(this.rover.getDirection()).run();
        break;
      default:
        throw new IllegalArgumentException("Unexpected input. || Key: " + input);
    }
  }

  public Rover getRover() {
    return rover;
  }
}
