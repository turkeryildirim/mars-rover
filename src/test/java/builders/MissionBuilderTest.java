package builders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.params.provider.Arguments.arguments;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import models.Mission;
import models.Plateau;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class MissionBuilderTest {

  MissionBuilder missionBuilder;

  static Stream<Arguments> acceptableCommands() {
    return Stream.of(
        arguments(Arrays.asList("5 5", "1 2 N", "M L M L M L M M")),
        arguments(Arrays.asList("5 5", "1 2 N", "M L M L M L M M"))
    );
  }

  @ParameterizedTest
  @MethodSource("acceptableCommands")
  void when_acceptableCommands_receive_missionBuilder_should_return_plateau(List<String> input) {
    Plateau expected = new Plateau(5, 5);

    missionBuilder = new MissionBuilder(input);
    Plateau actual = missionBuilder.getPlateau();

    assertEquals(expected.getHeight(), actual.getHeight());
    assertEquals(expected.getWidth(), actual.getWidth());
  }

  @ParameterizedTest
  @MethodSource("acceptableCommands")
  void when_acceptableCommands_receive_missionBuilder_should_return_x(List<String> input) {
    List<String> commands = Collections.singletonList("M, L, M, L, M, L, M, M");
    int expected = new Mission(1, 2, "N", commands).getStartX();

    missionBuilder = new MissionBuilder(input);
    int actual = missionBuilder.getMissions().get(0).getStartX();

    assertEquals(expected, actual);
  }

  @ParameterizedTest
  @MethodSource("acceptableCommands")
  void when_acceptableCommands_receive_missionBuilder_should_return_y(List<String> input) {
    List<String> commands = Collections.singletonList("M, L, M, L, M, L, M, M");
    int expected = new Mission(1, 2, "N", commands).getStartY();

    missionBuilder = new MissionBuilder(input);
    int actual = missionBuilder.getMissions().get(0).getStartY();

    assertEquals(expected, actual);
  }

  @ParameterizedTest
  @MethodSource("acceptableCommands")
  void when_acceptableCommands_receive_missionBuilder_should_return_startDirection(
      List<String> input) {
    List<String> commands = Collections.singletonList("M, L, M, L, M, L, M, M");
    String expected = new Mission(1, 2, "N", commands).getStartDirection();

    missionBuilder = new MissionBuilder(input);
    String actual = missionBuilder.getMissions().get(0).getStartDirection();

    assertEquals(expected, actual);
  }

  @ParameterizedTest
  @MethodSource("acceptableCommands")
  void when_acceptableCommands_receive_missionBuilder_should_return_commands(List<String> input) {
    List<String> commands = Collections.singletonList("M, L, M, L, M, L, M, M");
    String expected = new Mission(1, 2, "N", commands).getCommands().toString();

    missionBuilder = new MissionBuilder(input);
    String actual = missionBuilder.getMissions().get(0).getCommands().toString();

    assertEquals(expected, actual);
  }

  @Test
  void when_input_has_less_than_3_item_mission_builder_should_throw_illegalArgumentException() {
    assertThrows(IllegalArgumentException.class,
        () -> new MissionBuilder(Arrays.asList("5 5", "1 2 N")));
  }

  @Test
  void when_unacceptable_plateau_params_receive_missionBuilder_should_throw_illegalArgumentException() {
    assertThrows(IllegalArgumentException.class,
        () -> new MissionBuilder(Arrays.asList("5", "1 2 N", "M L M L M L M M")));
  }

  @Test
  void when_unacceptable_rover_params_receive_missionBuilder_should_throw_illegalArgumentException() {
    assertThrows(IllegalArgumentException.class,
        () -> new MissionBuilder(Arrays.asList("5 5", "1 N", "M L M L M L M M")));
  }
}
