package builders;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class RoverBuilderTest {

  RoverBuilder roverBuilder = new RoverBuilder(1, 1, "N");

  @ParameterizedTest
  @ValueSource(strings = {"R", "L", "M"})
  void when_acceptable_params_receive_move_should_change_rover_direction(String command) {
    String expected = roverBuilder.getRover().getLocationAsString();

    roverBuilder.move(command);

    String actual = roverBuilder.getRover().getLocationAsString();

    assertNotEquals(expected, actual);
  }

  @ParameterizedTest
  @NullSource
  @EmptySource
  @ValueSource(strings = {" ", "   ", "\t", "\n", "A", "r"})
  void assertThrowsException(String command) {
    assertThrows(IllegalArgumentException.class, () -> roverBuilder.move(command));
  }
}
